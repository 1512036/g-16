#pragma once
#include <iostream>
using namespace std;

template <class T>
class PRIORITY_QUEUE
{
	//hang doi uu tien su dung dslk don
private:
	struct Node
	{
		T key;
		Node* pNext;
	};
	Node* head;
	void swap(T &a, T &b)
	{
		T temp = a;
		a = b;
		b = temp;
	}
public:
	PRIORITY_QUEUE()
	{
		head = NULL;
	}
	PRIORITY_QUEUE(const PRIORITY_QUEUE &aQueue)
	{
		this->head = aQueue.head;
	}
	~PRIORITY_QUEUE() // destructor
	{
	
	}
	// operations
	bool isEmpty()
	{
		if (head == NULL)
		{
			return true;
		}
		return false;
	}
	//Them mot phan tu vao hang doi
	bool insert(T newItem)
	{
		//Tao mot node moi
		Node* p = new Node();
		if (p == NULL)
		{
			return false;
		}
		else
		{
			p->key = newItem;
			p->pNext = NULL;
			//Them node vao dslk don
			p->pNext = head;
			head = p;
			//build lai dslk don theo thu tu tang dan
			for (Node* m = head; m != NULL; m = m->pNext)
			{
				for (Node* n = m->pNext; n != NULL; n = n->pNext)
				{
					if (n->key < m->key)
					{
						swap(n->key, m->key);
					}
				}
			}
			return true;
		}

	}
	//Xoa phan tu nho nhat trong hang doi
	bool deleteMin(T &item)
	{
		if (head == NULL)
		{
			return false;
		}
		else
		{
			Node* p = head;
			item = head->key;
			head = head->pNext;
			delete p;
			return true;
		}
	}
	//Tra ve phan tu nho nhat trong hang doi ma khong xoa no
	bool minValue(T &item)
	{
		if (head == NULL)
		{
			return false;
		}
		else
		{
			Node* p = head;
			item = head->key;
			return true;
		}
	}
	//Xuat hang doi ra man hinh
	void showList()
	{
		for (Node* p = head; p != NULL; p=p->pNext)
		{
			cout << p->key << "   ";
		}
		cout << endl;
	}
}; // end class Priority Queue