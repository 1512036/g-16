#include <iostream>
#include "PQueue.h"
#include <fstream>
#include <string>

using namespace std;

void readingFile(string tenFile, BST<DonThuc> &DT)
{
	string str_DT;
	
	fstream f;
	f.open(tenFile, ios_base::in);
	getline(f, str_DT);
	f.close();
	vector<string> v1 = xuLyChuoiDaThuc(str_DT);
	vector<heSo> v = xuLyDonThuc(v1);
	for (int i = 0; i < v.size(); i++) {
		DonThuc dt(v[i].M_, v[i].X_, v[i].N_);
		DT.insert(dt);
	}
}

void writingFile(string tenFile, BST<DonThuc> &DT)
{
	string str_DT = "";
	fstream f;
	f.open(tenFile, ios_base::out | ios_base::app);
	str_DT = DT.xuat();
	f << str_DT << endl;
	f.close();
}

int main() {
	//NOTE: Doc, ghi va xu li file duoc su dung cac thu vien co san
	//BST<DonThuc> DT1, DT2;
	//readingFile("F1.txt", DT1);
	//readingFile("F2.txt", DT2);
	//cout << "Da Thuc 1: " << DT1.xuat() << "\n" << "Da Thuc 2: " << DT2.xuat() << "\n";
	//DT1.RutGon();
	//DT2.RutGon();

	//cout << "Sau khi rut gon:" << endl;
	//cout << "Da Thuc 1: " << DT1.xuat() << "\n" << "Da Thuc 2: " << DT2.xuat() << "\n";

	///*cout << "Da Thuc 1: " << DT1.xuat() << "\n" << "Da Thuc 2: " << DT2.xuat() << "\n";
	//DT1 = DT1 + DT2;
	//cout <<"Da Thuc 1 + 2:"<< DT1.xuat() << "\n";*/
	//DT1 = DT1 - DT2;
	//cout << "Da Thuc 1 - 2:" << DT1.xuat() << "\n";
	//system("pause");

	BST<DonThuc> DT1, DT2;
	readingFile("F1.txt", DT1);
	readingFile("F2.txt", DT2);
	DT1.RutGon();
	DT2.RutGon();
	string str_DT = "";
	fstream f;
	f.open("result.txt", ios_base::out);
	//In F1 sau khi rut gon va chuan hoa
	str_DT = DT1.xuat();
	f << "F1" << endl;
	f << str_DT << endl;
	//In F2 sau khi rut gon va chuan hoa
	str_DT = DT2.xuat();
	f << "F2" << endl;
	f << str_DT << endl;
	//In F3 = F1 + F2
	BST<DonThuc> DT3 = DT1 + DT2;
	str_DT = DT3.xuat();
	f << "F3" << endl;
	f << str_DT << endl;
	//In F4 = F1 - F2
	BST<DonThuc> DT4 = DT1 - DT2;
	str_DT = DT4.xuat();
	f << "F4" << endl;
	f << str_DT << endl;
	f.close();
	return 0;
}