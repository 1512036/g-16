﻿#pragma once
#include <iostream>
#include <string>
#include <vector>

using namespace std;

//**************************
//cấu trúc stack

//node stack
template <class T>
class SNode {
public:
	T data_;

	SNode<T>* pNext_;

	SNode() {
		pNext_ = NULL;
	}

	SNode(T key) {
		this->data_ = key;
		pNext_ = NULL;
	}

	~SNode() {}
};

//stack
template <class T>
class Stack {

	SNode<T>* pHead_; //con trỏ đầu

	//thêm 1 node vào stack
	void pushNode(SNode<T>* newNode) {
		newNode->pNext_ = pHead_;
		pHead_ = newNode;
		size_++;
	}

public:
	int size_;

	//thêm 1 key vào hàng đợi
	void push(T data) {
		SNode<T>* newNode = new SNode<T>(data);
		this->pushNode(newNode);
	}

	//lấy giá trị key đầu hàng đợi và xóa node đầu
	T pop() {
		T data = this->pHead_->data_;
		SNode<T>* pTemp = this->pHead_;
		this->pHead_ = this->pHead_->pNext_;
		delete pTemp; pTemp = NULL;
		size_--;
		return data;
	}

	//lấy giá trị key đầu hàng đợi
	T top() {
		return this->pHead_->data_;
	}

	//hàng đợi rỗng
	bool isEmpty() {
		if (this->pHead_ != NULL)
			return false;
		return true;
	}

	//lấy kích thước hàng đợi
	int getSize() {
		return size_;
	}

	//khởi tạo mặc định
	Stack() {
		this->pHead_ = NULL;
		size_ = 0;
	}

	//hàm hủy
	~Stack() {
		while (!this->isEmpty())
			this->pop();
		size_ = 0;
	}

	//khởi tạo copy
	Stack(const Stack<T>& p) {
		SNode<T>* pTemp = p.pHead_;
		this->pHead_ = new SNode<T>(pTemp->data_);
		SNode<T>* QTemp = this->pHead_;
		while ((pTemp = pTemp->pNext_) != NULL) {
			QTemp->pNext_ = new SNode<T>(pTemp->data_);
			QTemp = QTemp->pNext_;
		}
		size_ = p.size_;
	}

	Stack<T>& operator=(const Stack<T>& p) {
		SNode<T>* pTemp = p.pHead_;
		this->pHead_ = new SNode<T>(pTemp->data_);
		SNode<T>* QTemp = this->pHead_;
		while ((pTemp = pTemp->pNext_) != NULL) {
			QTemp->pNext_ = new SNode<T>(pTemp->data_);
			QTemp = QTemp->pNext_;
		}
		size_ = p.size_;
		return *this;
	}
};

//**************************
//cấu trúc hàng đợi ưu tiên

//node hàng đợi ưu tiên
template <class T>
class QNode {
public:
	T data_;

	QNode<T>* pNext_;

	QNode() {
		pNext_ = NULL;
	}

	QNode(T key) {
		this->data_ = key;
		pNext_ = NULL;
	}

	~QNode() {}

	//hoán vị data của 2 node kế tiếp nhau
	void swapData(QNode<T>* &pNext) {
		swap(pNext->data_, this->data_);
	}
};

//hàng đợi ưu tiên
template <class T>
class PQueue {

	QNode<T>* pHead_; //con trỏ đầu

	//thêm 1 node vào hàng đợi
	void enQueueNode(QNode<T>* newNode) {
		newNode->pNext_ = pHead_;
		pHead_ = newNode;
		QNode<T>* pTemp = pHead_;
		while (pTemp->pNext_ != NULL) {
			if (pTemp->data_ > pTemp->pNext_->data_)
				pTemp->swapData(pTemp->pNext_);
			pTemp = pTemp->pNext_;
		}
		size_++;
	}

public:
	int size_;

	//thêm 1 key vào hàng đợi
	void enQueue(T data) {
		QNode<T>* newNode = new QNode<T>(data);
		this->enQueueNode(newNode);
	}

	//lấy giá trị key đầu hàng đợi và xóa node đầu
	T deQueue() {
		T data = this->pHead_->data_;
		QNode<T>* pTemp = this->pHead_;
		this->pHead_ = this->pHead_->pNext_;
		delete pTemp; pTemp = NULL;
		size_--;
		return data;
	}

	//lấy giá trị key đầu hàng đợi
	T front() {
		return this->pHead_->data_;
	}

	//hàng đợi rỗng
	bool isEmpty() {
		if (this->pHead_ != NULL)
			return false;
		return true;
	}

	//lấy kích thước hàng đợi
	int getSize() {
		return size_;
	}

	//khởi tạo mặc định
	PQueue() {
		this->pHead_ = NULL;
		size_ = 0;
	}

	//hàm hủy
	~PQueue() {
		while (!this->isEmpty())
			this->deQueue();
		size_ = 0;
	}

	//khởi tạo copy
	PQueue(const PQueue<T>& p) {
		QNode<T>* pTemp = p.pHead_;
		this->pHead_ = new QNode<T>(pTemp->data_);
		QNode<T>* QTemp = this->pHead_;
		while ((pTemp = pTemp->pNext_) != NULL) {
			QTemp->pNext_ = new QNode<T>(pTemp->data_);
			QTemp = QTemp->pNext_;
		}
		size_ = p.size_;
	}


	PQueue<T>& operator=(const PQueue<T>& p) {
		QNode<T>* pTemp = p.pHead_;
		this->pHead_ = new QNode<T>(pTemp->data_);
		QNode<T>* QTemp = this->pHead_;
		while ((pTemp = pTemp->pNext_) != NULL) {
			QTemp->pNext_ = new QNode<T>(pTemp->data_);
			QTemp = QTemp->pNext_;
		}
		size_ = p.size_;
		return *this;
	}
};

//******************************************
//cấu trúc đa thức

//cấu trúc x^n
//lưu giá trị x và số mũ n trong đơn thức m * x ^ n
class SXN {
public:
	unsigned int _X; //Gía trị X 
	unsigned int _N; //Số mũ 

	SXN() {
		_X = _N = 0;
	}

	SXN(const SXN& s) {
		_X = s._X;
		_N = s._N;
	}

	SXN(unsigned int x, unsigned int n) {
		_X = x;
		_N = n;
	}

	~SXN() {}

	//3 hàm so sánh theo số mũ
	//nếu số mũ bằng nhau thì xét đến hệ số x
	bool operator>(const SXN& s) {
		if (this->_N > s._N)
			return true;
		else if (this->_N == s._N) {
			if (this->_X > s._X)
				return true;
		}
		return false;
	}

	bool operator<(const SXN& s) {
		if (this->_N < s._N)
			return true;
		else if (this->_N == s._N) {
			if (this->_X < s._X)
				return true;
		}
		return false;
	}

	bool operator==(const SXN& s) {
		if (this->_N == s._N && this->_X == s._X)
			return true;
		return false;
	}

	//xuất ra string với định dạng x^n
	string xuat() {
		string s;
		char buf[20] = "";
		_itoa_s(_X, buf, 10);
		s = buf; s += "^";
		_itoa_s(_N, buf, 10);
		s += buf;
		return s;
	}
};

//toán tử trích
ostream& operator<<(ostream& out, SXN s) {
	string str = s.xuat();
	out << str;
	return out;
}

//so sánh 2 hàng đợi theo số mũ và hệ số x ở đầu hàng đợi
//trả về -1 nếu s1 lớn hơn, 1 nếu s2 lớn hơn, 0 nếu bằng nhau
int PQueueCMP(PQueue<SXN> s1, PQueue<SXN> s2) {
	SXN sxn1 = s1.front();
	SXN sxn2 = s2.front();
	if (sxn1 > sxn2)
		return -1;
	else if (sxn1 < sxn2)
		return 1;
	return 0;
}

//cấu trúc 1 đơn thức m * x1 ^ n1 * x2 ^ n2
//lưu đơn thức có dạng trên khi thực hiện phép nhân
class CDThuc {
public:
	float M_; //tham số float m
	PQueue<SXN> xn_;

	//lấy giá trị x^n đầu
	SXN getXN() {
		return xn_.front();
	}

	CDThuc(float m, unsigned int x, unsigned int n) {
		SXN xn(x, n);
		this->xn_.enQueue(xn);
		this->M_ = m;
	}

	CDThuc() {
		this->M_ = 0;
	}

	~CDThuc() {}

	//3 hàm so sánh theo số mũ và hệ số m
	bool operator>(CDThuc d) {
		int i = PQueueCMP(this->xn_, d.xn_);
		if (i == -1)
			return true;
		return false;
	}

	bool operator<(CDThuc d) {
		int i = PQueueCMP(this->xn_, d.xn_);
		if (i == 1)
			return true;
		return false;
	}

	bool operator==(CDThuc d) {
		int i = PQueueCMP(this->xn_, d.xn_);
		if (i == 0)
			return true;
		return false;
	}

	//xuất ra định dạng m * x1 ^ n1 hoặc m * x1 ^ n1 * x2 ^ n2 tùy trường hợp
	string xuat() {
		char buf[20] = "";
		_snprintf_s(buf, 19, "%f", this->M_);
		string s = buf;
		string sTemp;
		//bỏ số phía sau float m
		bool flag = true;
		for (int i = s.length() - 1; i >= 0; i--) {
			if (s[i] != '0')
				flag = false;
			if (!flag) {
				if (s[i] == '.')
					continue;
				for (int j = i; j >= 0; j--, i--)
					sTemp = s[i] + sTemp;
				break;
			}
		}
		s = sTemp;
		s += "*";
		PQueue<SXN> pQTemp(this->xn_);
		SXN xnTemp = pQTemp.deQueue();
		s += xnTemp.xuat();
		if (pQTemp.isEmpty())
			return s;
		s += "*";
		s += pQTemp.deQueue().xuat();
		return s;
	}

	//cùng dạng, có cùng dạng m * x ^ n hoặc m * x1 ^ n1 * x2 ^ n2
	bool laCongDuoc(CDThuc cdt) {
		int size1 = this->xn_.getSize();
		int size2 = cdt.xn_.getSize();
		if (size1 == size2) {
			if (size1 == 1) {
				SXN sxn1 = this->xn_.front();
				SXN sxn2 = cdt.xn_.front();
				if (sxn1 == sxn2)
					return true;
			}
			else {
				PQueue<SXN> sTemp = this->xn_;
				sTemp.deQueue();
				cdt.xn_.deQueue();
				SXN sxn1 = this->xn_.front();
				SXN sxn2 = cdt.xn_.front();
				if (sxn1 == sxn2)
					return true;
			}
		}
		return false;
	}

	//toán tử cộng, chỉ sử dụng cho các đơn thức có cùng số mũ n và hệ số x
	CDThuc& operator+(const CDThuc& cdt) {
		this->M_ += cdt.M_;
		return *this;
	}

	CDThuc& operator*(const CDThuc& cdt){
		CDThuc temp(cdt);
		this->M_ *= temp.M_;
		this->xn_.enQueue(temp.xn_.deQueue());
		return *this;
	}
};

ostream& operator<<(ostream& out, CDThuc d) {
	string str = d.xuat();
	out << str;
	return out;
}

//cấu trúc các đơn thức có cùng số mũ n
//lưu các đơn thức có cùng số mũ n như m1 * x1 ^ n + m2 * x2 ^ n + ...
class DonThuc {
	DonThuc& nhanCDonThuc(const CDThuc& d) {
		PQueue<CDThuc> dt(this->donThuc_);
		PQueue<CDThuc> tich;
		while (!dt.isEmpty()) {
			CDThuc temp = dt.deQueue();
			temp = temp * d;
			tich.enQueue(temp);
		}
		this->donThuc_ = tich;
		return *this;
	}
public:
	unsigned int N_; //số mũ n
	PQueue<CDThuc> donThuc_; //dùng hàng đợi ưu tiên nên sẽ đc sắp xếp theo hệ số x từ nhỏ đến lớn

	DonThuc() {
		N_ = 0;
	}

	DonThuc(float m, unsigned int x, unsigned int n) {
		CDThuc d(m, x, n);
		this->N_ = n;
		this->donThuc_.enQueue(d);
	}

	DonThuc(const DonThuc& d) {
		N_ = d.N_;
		donThuc_ = d.donThuc_;
	}

	~DonThuc() {}

	//cộng 2 đơn thức
	DonThuc& operator+(const DonThuc& d) {
		DonThuc dTemp(d);
		while (!dTemp.donThuc_.isEmpty()) {
			this->donThuc_.enQueue(dTemp.donThuc_.deQueue());
		}
		this->congDonDThuc();
		return *this;
	}

	DonThuc& operator*(const DonThuc& d){
		PQueue<CDThuc> pQDThuc(d.donThuc_);
		PQueue<DonThuc> tong;
		while (!pQDThuc.isEmpty()) {
			DonThuc dt(*this);
			CDThuc temp = pQDThuc.deQueue();
			dt.nhanCDonThuc(temp);
			tong.enQueue(dt);
		}
		DonThuc dt1 = tong.deQueue();
		while (!tong.isEmpty()) {
			DonThuc dt2 = tong.deQueue();
			dt1 = dt1 + dt2;
		}
		this->donThuc_ = dt1.donThuc_;
		return *this;
	}

	bool operator>(const DonThuc& d) {
		if (this->N_ > d.N_)
			return true;
		if (this->N_ == d.N_) {
			PQueue<CDThuc> pQ1 = d.donThuc_;
			PQueue<CDThuc> pQ2 = this->donThuc_;
			if (pQ1.deQueue() < pQ2.deQueue()) {
				return true;
			}
		}
		return false;
	}

	bool operator<(const DonThuc& d) {
		if (this->N_ < d.N_)
			return true;
		if (this->N_ == d.N_) {
			PQueue<CDThuc> pQ1 = d.donThuc_;
			PQueue<CDThuc> pQ2 = this->donThuc_;
			if (pQ1.deQueue() > pQ2.deQueue()) {
				return true;
			}
		}
		return false;
	}

	bool operator==(const DonThuc& d) {
		DonThuc dTemp(d);
		PQueue<CDThuc> pQ1 = d.donThuc_;
		PQueue<CDThuc> pQ2 = this->donThuc_;
		if (this->N_ == d.N_) {
			if (pQ1.deQueue() == pQ2.deQueue()) {
				if (pQ1.deQueue() == pQ2.deQueue())
					return true;
			}
		}
		return false;
	}

	//xuất ra string với định dạng m1 * x1 ^ n + m2 * x2 ^ n + ...
	string xuat() {
		PQueue<CDThuc> d(this->donThuc_);
		string s = d.deQueue().xuat();
		while (!d.isEmpty()) {
			CDThuc temp = d.deQueue();
			string stemp = temp.xuat();
			if (stemp[0] != '-')
				s = s + "+" + stemp;
			else
				s = s + stemp;
		}
		return s;
	}

	//thêm 1 đơn thức m * x ^ n vào 
	//chỉ sử dụng với cùng số mũ n hoặc hàng đợi đơn thức rỗng
	void inSert(CDThuc d) {
		this->N_ = d.getXN()._N;
		this->donThuc_.enQueue(d);
	}
	
	//hàm bỏ các đơn thức có tham số float m = 0
	//nếu dãy đơn thức đều có tham số m = 0, thì giữ lại 1 đại diện duy nhất
	void rutGonDT0() {
		PQueue<CDThuc> d;
		CDThuc cDT;
		while (!this->donThuc_.isEmpty()) {
			cDT = this->donThuc_.deQueue();
			if (cDT.M_ != 0)
				d.enQueue(cDT);
		}
		if (d.isEmpty()) {
			CDThuc dt(0, 0, 0);
			d.enQueue(dt);
		}
		this->donThuc_ = d;
	}

	//cộng dồn các đơn thức có cùng hệ số
	void congDonDThuc() {
		PQueue<CDThuc> d;
		CDThuc cDT1 = this->donThuc_.deQueue();
		bool flag = true;
		while (!this->donThuc_.isEmpty()) {
			flag = false;
			CDThuc cDT2 = this->donThuc_.deQueue();
			if (cDT1.laCongDuoc(cDT2)) {
				cDT1 = cDT1 + cDT2;
				if (this->donThuc_.isEmpty()) {
					d.enQueue(cDT1);
					break;
				}
			}
			else {
				d.enQueue(cDT1);
				cDT1 = cDT2;
				if (this->donThuc_.isEmpty()) {
					d.enQueue(cDT1);
					break;
				}
			}
		}
		if (flag)
			d.enQueue(cDT1);
		this->donThuc_ = d;
		rutGonDT0();
	}

	void doiDauDonThuc() {
		PQueue<CDThuc> p;
		while (!this->donThuc_.isEmpty()) {
			CDThuc d = this->donThuc_.deQueue();
			d.M_ *= -1;
			p.enQueue(d);
		}
		this->donThuc_ = p;
	}

	bool bangKhong() {
		PQueue<CDThuc> p = this->donThuc_;
		CDThuc temp = p.deQueue();
		CDThuc khong(0, 0, 0);
		if (p.isEmpty() && temp == khong)
			return true;
		return false;
	}
};

ostream& operator<<(ostream& out, DonThuc d) {
	string str = d.xuat();
	out << str;
	return out;
}

//cấu trúc node BST
template <class T>
class TNode {
public:
	T _data;
	TNode<T>* _left;
	TNode<T>* _right;

	TNode() {
		_left = _right = NULL;
	}

	TNode(const T& data) {
		this->_data = data;
		this->_left = this->_right = NULL;
	}

	~TNode() {}

	//3 hàm so sánh 2 node theo số mũ và hệ số x
	bool operator>(TNode<T>* p) {
		if (this->_data > p->_data)
			return true;
		return false;
	}

	bool operator<(TNode<T>* p) {
		if (this->_data < p->_data)
			return true;
		return false;
	}

	bool operator==(TNode<T>* p) {
		if (this->_data == p->_data)
			return true;
		return false;
	}

	//xuất ra string đã được định dạng
	string xuat() {
		return this->_data.xuat();
	}

	//so sánh 2 node 
	int cmpData(TNode<T>* p) {
		if (p->_data > this->_data) return 1;
		if (p->_data < this->_data) return -1;
		return 0;
	}

	//trộn 2 node lại với nhau với các node có cùng số mũ
	//tham số truyền vào là m * x ^ n (data)
	TNode<T>& addData(T data) {
		this->_data = this->_data + data;
		return *this;
	}

	//đổi dấu đơn thức
	void doiDau() {
		this->_data.doiDauDonThuc();
	}

	/*TNode<T>& multData(T data) {
		this->_data = this->_data * data;
		return *this;
	}*/
};

template <class T>
ostream& operator<<(ostream& out, TNode<T> d) {
	string str = d.xuat();
	out << str;
	return out;
}

template <class T>
class BST {

	TNode<T>* _roof;

	//seach replacement TNode to delete
	//by the way the most right of left child tree
	bool seachTNode(TNode<T>* &preTNode, int& i, TNode<T>* src) {
		TNode<T>* roof = this->_roof;
		preTNode = this->_roof;
		if (!roof->cmpData(src))
			return true;
		while (roof != NULL) {
			if (roof->cmpData(src) == -1) {
				preTNode = roof; i = -1;
				roof = roof->_left;
			}
			else if (roof->cmpData(src) == 1) {
				preTNode = roof; i = 1;
				roof = roof->_right;
			}
			else
				return true;
		}
		return false;
	}

	//check TNode is leaf
	bool isLeaf(TNode<T>* src) {
		if (src->_left == NULL && src->_right == NULL)
			return true;
		return false;
	}

	//check TNode is roof
	bool isRoof(TNode<T>* src) {
		if (!src->cmpData(this->_roof))
			return true;
		return false;
	}

	//check the exist of child TNode
	//having left child TNode, index's value = -1
	//having right child TNode, index's value = 1
	//else index's value = 0
	bool isOnlyChildTNode(TNode<T>* src, int& i) {
		if (src->_left != NULL && src->_right == NULL) {
			i = -1;
			return true;
		}
		if (src->_left == NULL && src->_right != NULL) {
			i = 1;
			return true;
		}
		return false;
	}

	//delete leaf of BST
	bool delLeaf(TNode<T>* node) {
		if (this->isRoof(node)) {
			delete node;
			this->_roof = node = NULL;
			return true;
		}
		TNode<T>* preTNode = NULL; int i = 0;
		if (seachTNode(preTNode, i, node)) {
			if (i == -1) {
				delete node;
				node = NULL;
				preTNode->_left = NULL;
			}
			else if (i == 1) {
				delete node;
				node = NULL;
				preTNode->_right = NULL;
			}
			return true;
		}
		return false;
	}

	//delete TNode has only child TNode
	bool delTNodeOnlyChildTNode(TNode<T>* node, int i = 0) {
		TNode<T>* preTNode;
		if (this->isOnlyChildTNode(node, i)) {
			int j = 0;
			if (seachTNode(preTNode, j, node)) {
				if (i == -1 && j == -1) {
					preTNode->_left = node->_left;
					delete node;
					node = NULL;
				}
				else if (i == -1 && j == 1) {
					preTNode->_right = node->_left;
					delete node;
					node = NULL;
				}
				else if (i == 1 && j == -1) {
					preTNode->_left = node->_right;
					delete node;
					node = NULL;
				}
				else if (i == 1 && j == -1) {
					preTNode->_right = node->_right;
					delete node;
					node = NULL;
				}
				else if (i == 1 && j == 0) {
					this->_roof = node->_right;
					delete node;
					node = NULL;
				}
				else if (i == -1 && j == 0) {
					this->_roof = node->_left;
					delete node;
					node = NULL;
				}
			}
			return true;
		}
		return false;
	}

	//delete TNode 
	bool delTNode(TNode<T>* node, int i = 0) {
		if (isLeaf(node))
			return delLeaf(node);
		else if (isOnlyChildTNode(node, i))
			return delTNodeOnlyChildTNode(node);
		else {
			TNode<T>* altTNode = NULL;
			this->getMostLeftTNode(node, altTNode);
			//node->setData(altTNode->getData());
			node->_data = altTNode->_data;
			if (isLeaf(altTNode))
				return delLeaf(altTNode);
			else
				return delTNodeOnlyChildTNode(altTNode);
		}
		return false;
	}

	void getMostLeftTNode(TNode<T>* &node) {
		if (this->_roof == NULL)
			node = NULL;
		TNode<T>* roof = this->_roof;
		while (roof->_left != NULL)
			roof = roof->_left;
		node = roof;
	}

	void getMostLeftTNode(TNode<T>* roof, TNode<T>* &node) {
		if (roof == NULL)
			node = NULL;
		roof = roof->_right;
		while (roof->_left != NULL)
			roof = roof->_left;
		node = roof;
	}

	void insertTNode(TNode<T>* p, int i = 0) {
		if (this->isEmpty())
			this->_roof = p;
		else {
			TNode<T>* preTNode = NULL;
			TNode<T>* curTNode = this->_roof;
			while (curTNode != NULL) {
				if (curTNode->cmpData(p) == -1) {
					i = -1;
					preTNode = curTNode; curTNode = curTNode->_left;
				}
				else if (curTNode->cmpData(p) == 1) {
					i = 1;
					preTNode = curTNode; curTNode = curTNode->_right;
				}
				else if (curTNode->cmpData(p) == 0) {
					curTNode->addData(p->_data);
					return;
				}
			}
			if (i == -1)
				preTNode->_left = p;
			else if (i == 1)
				preTNode->_right = p;
		}
	}

	void copyTree(TNode<T>* roof, BST<T>& tree) {
		if (roof == NULL) {
			return;
		}
		tree.insert(roof->_data);
		copyTree(roof->_left, tree);
		copyTree(roof->_right, tree);
	}

	void getDonThuc(TNode<T>* roof, PQueue<T>& pQDT) {
		if (roof == NULL)
			return;
		getDonThuc(roof->_left, pQDT);
		pQDT.enQueue(roof->_data);
		getDonThuc(roof->_right, pQDT);
	}

public:
	TNode<T>* getRoof() {
		return this->_roof;
	}

	//cộng đa thức 
	void congDaThuc(BST<T>& dt1) {
		PQueue<T> data;
		getDonThuc(dt1.getRoof(), data);
		while (!data.isEmpty())
			this->insert(data.deQueue());
		this->RutGon();
	}

	void truDaThuc(BST<T>& dt1) {
		PQueue<T> data;
		getDonThuc(dt1.getRoof(), data);
		while (!data.isEmpty()) {
			DonThuc temp = data.deQueue();
			temp.doiDauDonThuc();
			this->insert(temp);
		}
		this->RutGon();
	}

	void nhanDaThucvoiDT(TNode<DonThuc>* p, const DonThuc& d){
		if (p == NULL)
			return;
		nhanDaThucvoiDT(p->_left, d);
		p->_data = p->_data * d;
		nhanDaThucvoiDT(p->_right, d);
	}

	void nhan(BST<T>& t) {
		PQueue<DonThuc> data;
		getDonThuc(t.getRoof(), data);
		Stack<BST<T>> tong;
		while (!data.isEmpty()) {
			BST<T> treeTemp;
			copyTree(treeTemp, *this);
			DonThuc d = data.deQueue();
			treeTemp.nhanDaThucvoiDT(treeTemp.getRoof(), d);
			tong.push(treeTemp);
		}
		BST<T> tree1 = tong.pop();
		while (!tong.isEmpty()) {
			BST<T> tree2 = tong.pop();
			tree1 = tree1 + tree2;
		}
		this->removeAll();
		PQueue<DonThuc> data1;
		getDonThuc(tree1.getRoof(), data1);
		while (!data1.isEmpty()) {
			this->insert(data1.deQueue());
		}
	}

	//constructor default
	BST() {
		this->_roof = NULL;
	}

	BST(const BST<T>& tree) {
		TNode<T>* p = tree._roof;
		this->_roof = NULL;
		copyTree(p, *this);
	}

	void removeAll() {
		while (!this->isEmpty()) {
			TNode<T>* p = NULL;
			this->getMostLeftTNode(p);
			this->delTNode(p);
		}
	}

	//destructor
	~BST() {
		while (!this->isEmpty()) {
			TNode<T>* p = NULL;
			this->getMostLeftTNode(p);
			this->delTNode(p);
		}
	}

	//check tree is empty
	bool isEmpty() {
		if (this->_roof != NULL)
			return false;
		return true;
	}

	//delele TNode with items
	void delTNode(T items) {
		TNode<T>* p = new TNode<T>(items);
		this->delTNode(p);
	}

	//insert TNode
	void insert(T items) {
		TNode<T>* p = new TNode<T>(items);
		this->insertTNode(p);
	}

	//Rut gon da thuc
	void RutGon() {
		PQueue<DonThuc> data;
		getDonThuc(this->_roof, data);
		this->removeAll();
		if (data.isEmpty()) {
			this->_roof = NULL;
			return;
		}
		else {
			while (!data.isEmpty()) {
				DonThuc dt = data.deQueue();
				dt.congDonDThuc();
				if (!dt.bangKhong())
					this->insert(dt);
			}
		}
	}

	BST& ChuanHoa() {}

	//Cong 2 da thuc
	BST& operator+(BST<T> Other){
		this->congDaThuc(Other);
		return *this;
	}

	//Tru 2 da thuc
	BST& operator-(BST<T> Other) {
		this->truDaThuc(Other);	
		return *this;
	}

	//Nhan 2 da thuc
	BST& operator*(BST<T> Other) {
		this->nhan(Other);
		return *this;
	}

	BST& operator=(BST<T> Other) {
		this->copyTree(*this, Other);
		return *this;
	}

	string xuat() {
		PQueue<DonThuc> p;
		getDonThuc(this->_roof, p);
		string sDaThuc;
		if (p.isEmpty()) {
			sDaThuc = "0";
			return sDaThuc;
		}
		DonThuc dt = p.deQueue();
		sDaThuc = dt.xuat();
		while (!p.isEmpty()) {
			DonThuc dt = p.deQueue();
			string temp = dt.xuat();
			if (temp[0] != '-')
				sDaThuc = sDaThuc + "+" + temp;
			else
				sDaThuc = sDaThuc + temp;
		}
		return sDaThuc;
	}

	friend ostream& operator<<(ostream &out, BST<T> x) {
		string str = x.xuat();
		out << str;
		return out;
	}

	void copyTree(BST<T> &dest, BST<T> src) {
		if (dest.getRoof() != NULL)
			dest.removeAll();
		PQueue<T> data;
		getDonThuc(src.getRoof(), data);
		while (!data.isEmpty()) {
			dest.insert(data.deQueue());
		}
	}

	bool operator==(BST<T>& d) {
		if (this->getRoof() == d.getRoof())
			return true;
		return false;
	}

	bool operator>(BST<T>& d) {
		if (this->getRoof() > d.getRoof())
			return true;
		return false;
	}

	bool operator<(BST<T>& d) {
		if (this->getRoof() < d.getRoof())
			return true;
		return false;
	}
};

void chuyenStr(char* &c, string str) {
	for (int i = 0; i < str.length() + 1; i++)
		c[i] = str[i];
}

void copyStrchar(char* &c, char* src) {
	int length = strlen(src);
	for (int i = 0; i < length + 1; i++)
		c[i] = src[i];
}

#include <vector>
vector<string> xuLyChuoiDaThuc(string sDaThuc) {
	bool dauTru = false;
	if (sDaThuc[0] == '-')
		dauTru = true;
	//chuyển sang char[]
	int length = sDaThuc.length();
	char* cDaThuc;
	cDaThuc = new char[sDaThuc.length() + 1];
	chuyenStr(cDaThuc, sDaThuc);
	//tách chuỗi với dấu - 
	vector<string> vString;
	char* context = NULL;
	while (strtok_s(cDaThuc, "-", &context)) {
		vString.push_back(cDaThuc);
		copyStrchar(cDaThuc, context);
	}
	context = NULL;
	delete[] cDaThuc;
	for (int i = 1; i < vString.size(); i++)
		vString[i] = "-" + vString[i];
	//tách chuỗi với dấu +
	vector<string> vStrDThuc;
	cDaThuc = new char[sDaThuc.length() + 1];
	for (int j = 0; j < vString.size(); j++) {
		chuyenStr(cDaThuc, vString[j]);
		if (strchr(cDaThuc, '+') != NULL) {
			while (strtok_s(cDaThuc, "+", &context)) {
				vStrDThuc.push_back(cDaThuc);
				copyStrchar(cDaThuc, context);
			}
		}
		else
			vStrDThuc.push_back(vString[j]);
	}
	delete[] cDaThuc;
	if (dauTru)
		vStrDThuc[0] = vStrDThuc[0] + "-";
	return vStrDThuc;
}

struct heSo {
	float M_;
	unsigned int N_;
	unsigned int X_;
};

heSo xuLyDThuc(string dt) {
	heSo hs;
	char buf[50] = "";
	strcpy_s(buf, dt.c_str());
	char* context = NULL;
	strtok_s(buf, "*", &context);
	string temp = buf;
	hs.M_ = stof(temp);
	strcpy_s(buf, context);
	if (strchr(buf, '^') == NULL) {
		hs.N_ = 1;
		hs.X_ = atoi(buf);
	}
	else {
		strtok_s(buf, "^", &context);
		hs.X_ = atoi(buf);
		hs.N_ = atoi(context);
	}
	return hs;
}

vector<heSo> xuLyDonThuc(vector<string>& vDonThuc) {
	heSo hs;
	vector<heSo> vHeSo;
	for (int i = 0; i < vDonThuc.size(); i++) {
		hs = xuLyDThuc(vDonThuc[i]);
		vHeSo.push_back(hs);
	}
	return vHeSo;
}
