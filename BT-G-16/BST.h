//#pragma once
//#include <iostream>
//using namespace std;
//
//template <class T> 
//class BSTNode{
//public:
//	T data; // data of node
//	BSTNode *left; // pointer to left child
//	BSTNode *right; // pointer to right child
//
//	BSTNode();
//	BSTNode(T item);
//}; // end class 
//
//
//template <class T> class BINARY_SEARCH_TREE {
//private:
//	BSTNode<T> *root; // pointer to root of tree
//
//	void LNR(BSTNode<T> *p);
//	void NLR(BSTNode<T> *p);
//	void LRN(BSTNode<T> *p);
//	int countNode(BSTNode<T> *p);
//	int height(BSTNode<T> *p);
//	bool insert(BSTNode<T> *&p, T newItem);
//	bool remove(BSTNode<T> *&p, T item);
//	BSTNode<T>* find_MaxNode_Left(BSTNode<T> *p); // find node with Max(data) in left child tree
//public:
//	BINARY_SEARCH_TREE(); // default constructor
//	BINARY_SEARCH_TREE(const BINARY_SEARCH_TREE &aTree); // copy constructor
//	~BINARY_SEARCH_TREE(); // destructor
//
//	// operations
//	bool isEmpty();
//	int countNode();
//	int height();
//	BSTNode<T>* findNode(T item); // find node with �item'
//	bool insert(T newItem);	// call insert(root, newItem)
//	bool remove(T item);	// call remove(root, item)
//	void preorder(); // call NLR(root)
//	void inorder(); // call LNR(root)
//	void postorder(); // call LRN(root)
//}; // end class
//
//
//template <class T>
//BSTNode<T>::BSTNode()
//{
//	/// ...
//}
//
//template <class T>
//BSTNode<T>::BSTNode(T item)
//{
//	data = item;
//	left = right = NULL;
//}
//
//template <class T>
//BINARY_SEARCH_TREE<T>::BINARY_SEARCH_TREE()
//{
//	root = NULL;
//}
//
//template <class T>
//BINARY_SEARCH_TREE<T>::BINARY_SEARCH_TREE(const BINARY_SEARCH_TREE &aTree)
//{
//	/// ...
//}
//
//template <class T>
//BINARY_SEARCH_TREE<T>::~BINARY_SEARCH_TREE()
//{
//	/// ...
//}
//
//template <class T>
//bool BINARY_SEARCH_TREE<T>::isEmpty()
//{
//	if (root == NULL)
//		return true;
//	return false;
//}
//
//template <class T>
//int BINARY_SEARCH_TREE<T>::countNode(BSTNode<T> *p)
//{
//	if (p == NULL)
//		return 0;
//	return 1 + countNode(p->left) + countNode(p->right);
//}
//
//template <class T>
//int BINARY_SEARCH_TREE<T>::countNode()
//{
//	return countNode(root);
//}
//
//template <class T>
//int BINARY_SEARCH_TREE<T>::height(BSTNode<T> *p)
//{
//	if (p == NULL)
//		return 0;
//	int a = height(p->left);
//	int b = height(p->right);
//	if (a > b)
//		return a + 1;
//	else
//		return b + 1;
//}
//
//template <class T>
//int BINARY_SEARCH_TREE<T>::height()
//{
//	return height(root);
//}
//
//template <class T>
//void BINARY_SEARCH_TREE<T>::LNR(BSTNode<T> *p)
//{
//	if (p == NULL)
//		return;
//	LNR(p->left);
//	cout << p->data << endl;
//	LNR(p->right);
//}
//
//template <class T>
//void BINARY_SEARCH_TREE<T>::inorder()
//{
//	LNR(root);
//}
//
//template <class T>
//void BINARY_SEARCH_TREE<T>::NLR(BSTNode<T> *p)
//{
//	if (p != NULL)
//	{
//		cout << p->data << endl;
//		NLR(p->left);
//		NLR(p->right);
//	}
//}
//
//template <class T>
//void BINARY_SEARCH_TREE<T>::preorder()
//{
//	NLR(root);
//}
//
//template <class T>
//void BINARY_SEARCH_TREE<T>::LRN(BSTNode<T> *p)
//{
//	if (p != NULL)
//	{
//		LRN(p->left);
//		LRN(p->right);
//		cout << p->data << endl;
//	}
//}
//
//template <class T>
//void BINARY_SEARCH_TREE<T>::postorder()
//{
//	LRN(root);
//}
//
//template <class T>
//BSTNode<T>* BINARY_SEARCH_TREE<T>::findNode(T item)
//{
//	if (isEmpty())
//		return NULL;
//
//	BSTNode<T> *q = root;
//	while (q != NULL)
//	{
//		if (q->data == item)
//			return q;
//		else if (item < q->data)
//			q = q->left;
//		else if (item > q->data)
//			q = q->right;
//	}
//	return NULL;
//}
//
//template <class T>
//bool BINARY_SEARCH_TREE<T>::insert(BSTNode<T> *&p, T newItem)
//{
//	// p la root
//	BSTNode<T> *q = new BSTNode<T>(newItem);
//
//	if (isEmpty())
//	{
//		p = q;
//		return true;
//	}
//
//	BSTNode<T> *curr = p;
//	while (curr != NULL)
//	{
//		if (newItem == curr->data)
//			return false;
//		else if (newItem < curr->data)
//		{
//			if (curr->left != NULL)
//				curr = curr->left;
//			else
//			{
//				curr->left = q;
//				return true;
//			}
//		}
//		else if (newItem > curr->data)
//		{
//			if (curr->right != NULL)
//				curr = curr->right;
//			else
//			{
//				curr->right = q;
//				return true;
//			}
//		}
//	}
//	return false;
//}
//
//template <class T>
//bool BINARY_SEARCH_TREE<T>::insert(T newItem)
//{
//	if (insert(root, newItem))
//		return true;
//	return false;
//}
//
//template <class T>
//BSTNode<T>* BINARY_SEARCH_TREE<T>::find_MaxNode_Left(BSTNode<T> *p)
//{
//	if (isEmpty())
//		return NULL;
//
//	BSTNode<T> *q = p->left;
//	while (q->right != NULL)
//		q = q->right;
//
//	return q;
//}
//
//template <class T>
//bool BINARY_SEARCH_TREE<T>::remove(BSTNode<T> *&p, T item)
//{
//	// p la root
//	if (isEmpty())
//		return false;
//
//	if (item == p->data && p->left == NULL && p->right == NULL)
//	{
//		BSTNode<T> *q = p;
//		p = NULL;
//		delete q;
//		return true;
//	}
//	else if (item == p->data && p->left != NULL && p->right == NULL)
//	{
//		BSTNode *q = p;
//		p = p->left;
//		delete q;
//		return true;
//	}
//	else if (item == p->data && p->left == NULL && p->right != NULL)
//	{
//		BSTNode *q = p;
//		p = p->right;
//		delete q;
//		return true;
//	}
//
//	BSTNode<T> *curr = p;	// Luu vet con tro hien tai
//	BSTNode<T> *prev = p;	// Luu vet con tro truoc do
//	if (item < curr->data)
//	{
//		prev = curr;
//		curr = curr->left;
//	}
//	else if (item > curr->data)
//	{
//		prev = curr;
//		curr = curr->right;
//	}
//	while (curr != NULL)
//	{
//		if (item == curr->data)
//		{
//			// Truong hop curr la node la
//			if (curr->left == NULL && curr->right == NULL)
//			{
//				if (curr->data < prev->data)
//				{
//					prev->left = NULL;
//					delete curr;
//					return true;
//				}
//				if (curr->data > prev->data)
//				{
//					prev->right = NULL;
//					delete curr;
//					return true;
//				}
//			}
//
//			// Truong hop curr chi co cay con phai
//			if (curr->left == NULL && curr->right != NULL)
//			{
//				if (curr->data < prev->data)
//				{
//					prev->left = curr->right;
//					delete curr;
//					return true;
//				}
//				if (curr->data > prev->data)
//				{
//					prev->right = curr->right;
//					delete curr;
//					return true;
//				}
//			}
//
//			// Truong hop curr chi co cay con trai
//			if (curr->left != NULL && curr->right == NULL)
//			{
//				if (curr->data < prev->data)
//				{
//					prev->left = curr->left;
//					delete curr;
//					return true;
//				}
//				if (curr->data > prev->data)
//				{
//					prev->right = curr->left;
//					delete curr;
//					return true;
//				}
//			}
//
//			// Truong hop curr co 2 cay con
//			if (curr->left != NULL && curr->right != NULL)
//			{
//				// Ta co 2 cach lam:
//				// 1: lay node co gia tri cao nhat o cay con trai len thay the
//				// 2: lay node co gia tri thap nhat o cay co phai len thay the
//				// O day ta chon cach 1
//				BSTNode<T> *max = find_MaxNode_Left(curr);
//				T temp = max->data;
//				remove(root, temp);
//				curr->data = temp;
//				return true;
//			}
//		}
//		else if (item < curr->data)
//		{
//			prev = curr;
//			curr = curr->left;
//		}
//		else if (item > curr->data)
//		{
//			prev = curr;
//			curr = curr->right;
//		}
//	}
//
//	return false;
//}
//
//template <class T>
//bool BINARY_SEARCH_TREE<T>::remove(T item)
//{
//	if (remove(root, item))
//		return true;
//	return false;
//}

//#pragma once
//#include <iostream>
//#include <string>
//#include <queue>
//#include <fstream>
//
//using namespace std;
//
////define struct TNode of binary seach tree
//
////define class binary seach tree BST

