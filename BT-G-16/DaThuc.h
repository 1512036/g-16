﻿//#ifndef _Header_h
//#define _Header_h
//
//#include <iostream>
//#include <fstream>
//using namespace std;
//
////// Cau truc du lieu da chon: Danh sach lien ket
////
//////Du lieu cua don thuc
////struct DuLieu
////{
////	float HeSo;
////	unsigned int X1;
////	unsigned int SoMu1;
////	unsigned int X2;
////	unsigned int SoMu2;
////};
////
//////Lop DonThuc dong vai tro la Node
////class DonThuc
////{
////public:
////	DuLieu data;		//Du Lieu cua 1 don thuc gom he so, so mu va gia tri x
////	DonThuc *pNext;		//Con tro den don thuc tiep theo
////
////	DonThuc();									//Default Constructor
////	DonThuc(const DonThuc &Other);				//Copy Constructor
////	DonThuc& operator=(const DonThuc &Other);	//Operator =
////	~DonThuc();									//Destructor
////
////	DonThuc operator+(const DonThuc &Other);								//Cong 2 don thuc
////	DonThuc operator-(const DonThuc &Other);								//Tru 2 don thuc	
////	DonThuc operator*(const DonThuc &Other);								//Nhan 2 don thuc
////};
////
//////Lop DaThuc f(x, n) la 1 danh sach lien ket
////class DaThuc
////{
////private:
////	DonThuc *pHead;			//Con tro den don thuc dau tien trong da thuc
////	int size;				//So phan tu cua da thuc
////
////	bool isEmpty();									//Kiem tra da thuc co = 0
////	void insert(int index, DuLieu newItem);			//Them 1 don thuc sau vi tri index
////	void remove(int index);							//Xoa don thuc o vi tri index
////	int find(DuLieu key);							//Tim don thuc
////public:
////	DaThuc();														//Default Constructor
////	DaThuc(const DaThuc &Other);									//Copy Constructor
////	DaThuc& operator=(const DaThuc &Other);							//Operator =
////	~DaThuc();														//Destructor
////	int getSize();													//Lay so phan tu cua da thuc
////	DaThuc& RutGon();												//Rut gon da thuc
////	DaThuc& ChuanHoa();												//Chuan hoa da thuc
////	DaThuc operator+(const DaThuc &Other);							//Cong 2 da thuc
////	DaThuc operator-(const DaThuc &Other);							//Tru 2 da thuc
////	DaThuc operator*(const DaThuc &Other);							//Nhan 2 da thuc
////};
//
//
//struct Node
//{
//	unsigned int X;				//Gía trị X
//	unsigned int N;				//Số mũ
//	Node *pNext;
//};
//
//class DonThuc
//{
//public:
//	//struct key
//	//{
//	//	float A;				//Hệ số
//	//	Node *pHead;
//	//};
//	Node *data;
//	DonThuc *pLeft;
//	DonThuc *pRight;
//
//
//	DonThuc();									//Default Constructor
//	DonThuc(const DonThuc &Other);				//Copy Constructor
//	DonThuc& operator=(const DonThuc &Other);	//Operator =
//	~DonThuc();									//Destructor	
//	const DonThuc operator+(const DonThuc &Other);	//Cong 2 don thuc
//	const DonThuc operator-(const DonThuc &Other);	//Tru 2 don thuc	
//	const DonThuc operator*(const DonThuc &Other);	//Nhan 2 don thuc
//
//	/*bool operator >(const DonThuc&) const;
//	bool operator <(const DonThuc&) const;
//	bool operator ==(const DonThuc&) const;*/
//};
//
//class DaThuc
//{
//private:
//	DonThuc *pRoot;			//Con tro den don thuc dau tien trong da thuc
//	int size;				//So phan tu cua da thuc
//
//	bool isEmpty();									//Kiem tra da thuc co = 0
//	bool insert(/*int index, Node newItem*/ DonThuc *&p, const DonThuc &newItem);			//Them 1 don thuc sau vi tri index
//	void remove(int index);							//Xoa don thuc o vi tri index
//	int find(Node key);							//Tim don thuc
//public:
//	DaThuc();														//Default Constructor
//	DaThuc(const DaThuc &Other);									//Copy Constructor
//	DaThuc& operator=(const DaThuc &Other);							//Operator =
//	~DaThuc();														//Destructor
//	int getSize();													//Lay so phan tu cua da thuc
//	DaThuc& RutGon();												//Rut gon da thuc
//	DaThuc& ChuanHoa();												//Chuan hoa da thuc
//	const DaThuc operator+(const DaThuc &Other);							//Cong 2 da thuc
//	const DaThuc operator-(const DaThuc &Other);							//Tru 2 da thuc
//	const DaThuc operator*(const DaThuc &Other);							//Nhan 2 da thuc
//};
//
//#endif